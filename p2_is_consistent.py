# -*- coding: utf-8 -*-

def is_consistent(csp, variable, value):
    for c in csp.constraints[variable]:
        # continue if variable has not been assigned yet
        if not c.var2 in csp.assignment:
            continue
        passed = False
        for v2 in c.var2.domain:
            # if satisfied for any var in v2's domain
            if c.is_satisfied(value, v2):
                passed = True
                break
        if not passed:
            return False
    return True
