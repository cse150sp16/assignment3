# -*- coding: utf-8 -*-

from collections import deque
import Queue as Q


def inference(csp, variable):
    """Performs an inference procedure for the variable assignment.

    For P6, *you do not need to modify this method.*
    """
    return ac3(csp, csp.constraints[variable].arcs())


def backtracking_search(csp):
    """Entry method for the CSP solver.  This method calls the backtrack method to solve the given CSP.

    If there is a solution, this method returns the successful assignment (a dictionary of variable to value);
    otherwise, it returns None.

    For P6, *you do not need to modify this method.*
    """
    if backtrack(csp):
        return csp.assignment
    else:
        return None


def select_unassigned_variable(csp):
    variable = None
    for v in csp.variables:
        if not v.is_assigned():
            variable = v
            break

    min_value = len(variable.domain)
    # max_degree = len(csp.constraints[variable])
    variables = [variable]

    for var in csp.variables:
        if var.is_assigned() or var == variable:
            continue
        value = len(var.domain)
        if value < min_value:
            min_value = value
            variables = [var]
            # max_degree = len(csp.constraints[var])
        elif value == min_value:
            if len(csp.constraints[var]) < len(csp.constraints[variables[0]]):
                variables = [var]
                # max_degree = len(csp.constraints[var])
    return variables[0]


def order_domain_values(csp, variable):
    q = Q.PriorityQueue()
    tail = []
    for value in variable.domain:
        # csp.variables.begin_transaction()
        # variable.assign(value)
        c = 0
        for constraint in csp.constraints[variable]:
            for v in constraint.var2.domain:
                if constraint.is_satisfied(value, v):
                    c += 1
                elif constraint.var2.is_assigned():
                    c = -1
                    break
            if c < 0:
                tail.append(value)
                # c = 0
                break
        # csp.variables.rollback()
        if c < 0:
            continue
        q.put((-1*c, value))
    domain = []
    while not q.empty():
        c, value = q.get()
        domain.append(value)

    domain += tail
    return domain


def backtrack(csp):
    if is_complete(csp):
        return True
		
	if not ac3(csp):
        return False
    elif is_complete(csp):
        return True

    variable = select_unassigned_variable(csp)
    if variable:
        for value in order_domain_values(csp, variable):
            if is_consistent(csp, variable, value):
                csp.variables.begin_transaction()
                variable.assign(value)
                if inference(csp, variable):
                    result = backtrack(csp)
                    if result:
                        return result
                csp.variables.rollback()

    return False


def is_complete(csp):
    for variable in csp.variables:
        if not variable.is_assigned():
            return False
    return True


def is_consistent(csp, variable, value):
    for constraint in csp.constraints[variable]:
        # variable.assign(value)
        if constraint.var2.is_assigned():
            if not constraint.is_satisfied(value, constraint.var2.domain[0]):
                return False
    return True


def ac3(csp, arcs=None):
    queue_arcs = deque(arcs if arcs is not None else csp.constraints.arcs())
    while len(queue_arcs):
        xi, xj = queue_arcs.popleft()
        if revise(csp, xi, xj):
            if len(xi.domain) == 0:
                return False
            for a, b in csp.constraints[xi].arcs():
                if b != xj and a == xi:
                    queue_arcs.append((b, xi))
    return True


def revise(csp, xi, xj):
    revised = False
    for c in csp.constraints[xi, xj]:
        for x in xi.domain:
            satisfied = False
            for y in xj.domain:
                if c.is_satisfied(x, y):
                    satisfied = True
                    break
            if not satisfied:
                xi.domain.remove(x)
                revised = True
    return revised
