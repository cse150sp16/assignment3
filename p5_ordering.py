# -*- coding: utf-8 -*-
import Queue


"""Selects the next unassigned variable, or None if there is no more unassigned variables
    (i.e. the assignment is complete).

    This method implements the minimum-remaining-values (MRV) and degree heuristic. That is,
    the variable with the smallest number of values left in its available domain.  If MRV ties,
    then it picks the variable that is involved in the largest number of constraints on other
    unassigned variables.
    """
def select_unassigned_variable(csp):
    csp_vars = csp.variables
    lowest_domain = 99999
    highest_degree = 0
    mrvVar = None
    for c in csp_vars:
        if  not c.is_assigned():
            if len(c.domain) < lowest_domain:
                lowest_domain = len(c.domain)
                highest_degree = len(csp.constraints[c])
                mrvVar = c
            elif len(c.domain) == lowest_domain:
                if len(csp.constraints[c]) > highest_degree:
                    highest_degree = len(csp.constraints[c])
                    mrvVar = c
    return mrvVar
    
    


"""Returns a list of (ordered) domain values for the given variable.

    This method implements the least-constraining-value (LCV) heuristic; that is, the value
    that rules out the fewest choices for the neighboring variables in the constraint graph
    are placed before others.
    """
def order_domain_values(csp, variable):
    pq = Queue.PriorityQueue()
    domainList = []
    for v in variable.domain:
        count = 0
        for c in csp.constraints[variable]:
            if v in c.var2.domain:
                count = count + 1
        pq.put((count, v))
    while not pq.empty():
        domainList.append(pq.get()[1])
    return domainList
    pass
