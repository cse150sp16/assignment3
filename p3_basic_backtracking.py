# -*- coding: utf-8 -*-

def select_unassigned_variable(csp):
    """Selects the next unassigned variable, or None if there is no more unassigned variables
    (i.e. the assignment is complete).

    For P3, *you do not need to modify this method.*
    """
    return next((variable for variable in csp.variables if not variable.is_assigned()))


def order_domain_values(csp, variable):
    """Returns a list of (ordered) domain values for the given variable.

    For P3, *you do not need to modify this method.*
    """
    return [value for value in variable.domain]


def inference(csp, variable):
    """Performs an inference procedure for the variable assignment.

    For P3, *you do not need to modify this method.*
    """
    return True


def backtracking_search(csp):
    """Entry method for the CSP solver.  This method calls the backtrack method to solve the given CSP.

    If there is a solution, this method returns the successful assignment (a dictionary of variable to value);
    otherwise, it returns None.

    For P3, *you do not need to modify this method.*
    """
    if backtrack(csp):
        return csp.assignment
    else:
        return None


def backtrack(csp):
    if is_complete(csp):
        return True

    var = select_unassigned_variable(csp)
    for val in order_domain_values(csp, var):
        if is_consistent(csp, var, val):
            csp.variables.begin_transaction()
            csp.assignment[var] = val
            var.domain = [x for x in var.domain if x != val]
            var.assign(val)
            if backtrack(csp):
                return True
            csp.variables.rollback()
        if var in csp.assignment:
            del csp.assignment[var]
    return False


# returns True when the variable assignment to value is consistent
def is_consistent(csp, variable, value):
    for c in csp.constraints[variable]:
        # continue if variable has not been assigned yet
        if not c.var2 in csp.assignment:
            continue
        passed = False
        for v2 in c.var2.domain:
            # if satisfied for any var in v2's domain
            if c.is_satisfied(value, v2):
                passed = True
                break
        if not passed:
            return False
    return True


# returns True when all variables in the CSP have been assigned
def is_complete(csp):
    csp_vars = csp.variables
    assigned = True
    for c in csp_vars:
        if not c.is_assigned():
            assigned = False

    return assigned