# -*- coding: utf-8 -*-



def is_complete(csp):
    csp_vars = csp.variables
    assigned = True
    for c in csp_vars:
        if not c.is_assigned():
            assigned = False

    return assigned